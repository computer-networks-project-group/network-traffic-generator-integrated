# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ICMPv4.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
import socket
from scapy.all import *
from scapy.layers.inet import IP, ICMP


class Ui_ICMPv4(object):
    def __int__(self):
        self.ipaddress = ""
        self.s_ipaddress = ""


    def setupUi(self, ICMPv4):
        ICMPv4.setObjectName("ICMPv4")
        ICMPv4.resize(860, 600)
        self.centralwidget = QtWidgets.QWidget(ICMPv4)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(110, 10, 521, 51))
        font = QtGui.QFont()
        font.setFamily("Sitka Text")
        font.setPointSize(18)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setCursor(QtGui.QCursor(QtCore.Qt.IBeamCursor))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(10, 100, 151, 31))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(10, 150, 155, 31))
        self.label_3.setObjectName("label_3")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(400, 100, 191, 31))
        self.label_5.setObjectName("label_5")
        self.sourceIP = QtWidgets.QLineEdit(self.centralwidget)
        self.sourceIP.setGeometry(QtCore.QRect(180, 110, 201, 20))
        self.sourceIP.setObjectName("sourceIP")
        self.packetNumber = QtWidgets.QLineEdit(self.centralwidget)
        self.packetNumber.setGeometry(QtCore.QRect(180, 150, 201, 20))
        self.packetNumber.setObjectName("sourcePort")
        self.destinationIP = QtWidgets.QLineEdit(self.centralwidget)
        self.destinationIP.setGeometry(QtCore.QRect(610, 110, 201, 20))
        self.destinationIP.setObjectName("destinationIP")
        self.generateBtn = QtWidgets.QPushButton(self.centralwidget)
        self.generateBtn.setGeometry(QtCore.QRect(704, 142, 111, 31))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(12)
        font.setBold(True)
        font.setItalic(True)
        font.setWeight(75)
        self.generateBtn.setFont(font)
        self.generateBtn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.generateBtn.setObjectName("generateBtn")
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(35, 280, 801, 251))
        self.listWidget.setObjectName("listWidget")
        self.clearBtn = QtWidgets.QPushButton(self.centralwidget)
        self.clearBtn.setGeometry(QtCore.QRect(750, 550, 91, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.clearBtn.setFont(font)
        self.clearBtn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.clearBtn.setObjectName("clearBtn")
        ICMPv4.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(ICMPv4)
        self.statusbar.setObjectName("statusbar")
        ICMPv4.setStatusBar(self.statusbar)

        self.retranslateUi(ICMPv4)
        QtCore.QMetaObject.connectSlotsByName(ICMPv4)

        self.generateBtn.clicked.connect(self.generate)
        self.clearBtn.clicked.connect(self.clearItems)

###################################### IP VALIDATION
    def is_valid_ipv4_address(self):
        print('98')
        try:
            socket.inet_pton(socket.AF_INET, self.ipaddress)
        except AttributeError:  # no inet_pton here, sorry
            try:
                print('roba1fggggggg222')
                socket.inet_aton(self.ipaddress)
            except socket.error:
                return False
            return self.ipaddress.count('.') == 3
        except socket.error:  # not a valid address
            return False
        print('roba122eeeeeeeee2')
        return True

############################################ SENDING TRAFFIC

    def pingflood(self):
        # s_add = RandIP()
        # s_add = "127.0.0.1"
        # d_add = ipaddress

        packet = IP(src=self.s_ipaddress, dst=self.ipaddress) / ICMP()
        send(packet)

#############################################################

    def clearItems(self):
        self.listWidget.clear()

    def generate(self):
        if not (self.sourceIP.text() == "" and
                self.packetNumber.text() == "" and self.destinationIP.text() == ""):
            self.listWidget.addItem("The source ip is: " + self.sourceIP.text())
            self.listWidget.addItem("The destination ip is: " + self.destinationIP.text())
            self.listWidget.addItem("The number of packets is: " + self.packetNumber.text())
            #self.sourceIP.setText("")
            #self.destinationIP.setText("")
            #self.packetNumber.setText("")
##################################################
            x = False

            self.ipaddress = self.destinationIP.text()
            self.s_ipaddress = self.sourceIP.text()
            self.numpack = self.packetNumber.text()
            print('roba1222')

            if self.ipaddress.count('.') == 3:
                print('roba1222')
                x = self.is_valid_ipv4_address()
            else:
                print("invalid ip address")
                self.listWidget.addItem("invalid ip address")

            if x == True:
                print("ip address is valid")
                self.listWidget.addItem("ip address is valid")
                p = 0
                while (p < int(self.numpack)):
                    p = p + 1
                    # dport = 53
                    # ipaddress = "192.168.43.1"
                    self.pingflood()
                    print(p, " packet sent.")
                    #self.listWidget.addItem(p, " packet sent.")

            else:
                print("ip address is not valid. please insert valid ip address")
                #self.listWidget.addItem("ip address is not valid. please insert valid ip address")

    def retranslateUi(self, ICMPv4):
        _translate = QtCore.QCoreApplication.translate
        ICMPv4.setWindowTitle(_translate("ICMPv4", "Internet Control Message Protocol"))
        self.label.setText(_translate("ICMPv4", "<html><head/><body><p><span style=\" "
                                                "font-size:16pt;\">Internet Control "
                                                "Message Protocol "
                                                "version-4</span></p></body></html>"))
        self.label_2.setText(_translate("ICMPv4", "<html><head/><body><p><span style=\" "
                                                  "font-size:12pt; "
                                                  "font-weight:600;\">Source Ip "
                                                  "Address</span></p></body></html>"))
        self.label_3.setText(_translate("ICMPv4", "<html><head/><body><p><span style=\" "
                                                  "font-size:12pt; "
                                                  "font-weight:600;\">Number of Packets"
                                                  "</span></p></body></html>"))
        self.label_5.setText(_translate("ICMPv4", "<html><head/><body><p><span style=\" "
                                                  "font-size:12pt; "
                                                  "font-weight:600;\">Destination Ip "
                                                  "Address</span></p></body></html>"))
        self.generateBtn.setText(_translate("ICMPv4", "Generate"))
        self.clearBtn.setText(_translate("ICMPv4", "Clear"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    icmp4 = QtWidgets.QMainWindow()
    ui = Ui_ICMPv4()
    ui.setupUi(icmp4)
    icmp4.show()
    sys.exit(app.exec())
