# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'UDPv4.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
import socket
import time
import random


class Ui_UDPv4(object):
    def __int__(self):
        self.address = ""
        self.PORTs = ""
        self.PORTr = ""
        self.pnum = ""

    def setupUi(self, UDPv4):
        UDPv4.setObjectName("UDPv4")
        UDPv4.resize(861, 600)
        self.centralwidget = QtWidgets.QWidget(UDPv4)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(120, 20, 421, 41))
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(20, 103, 151, 31))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(70, 140, 101, 31))
        self.label_3.setObjectName("label_3")
        self.sourceIP = QtWidgets.QLineEdit(self.centralwidget)
        self.sourceIP.setGeometry(QtCore.QRect(190, 110, 211, 20))
        self.sourceIP.setObjectName("sourceIP")
        self.sourcePort = QtWidgets.QLineEdit(self.centralwidget)
        self.sourcePort.setGeometry(QtCore.QRect(190, 150, 211, 20))
        self.sourcePort.setObjectName("sourcePort")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(430, 103, 191, 31))
        self.label_4.setObjectName("label_4")
        self.destinationIP = QtWidgets.QLineEdit(self.centralwidget)
        self.destinationIP.setGeometry(QtCore.QRect(630, 110, 211, 20))
        self.destinationIP.setObjectName("destinationIP")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(480, 140, 141, 31))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(430, 180, 161, 31))
        self.label_6.setObjectName("label_6")
        self.destinationPort = QtWidgets.QLineEdit(self.centralwidget)
        self.destinationPort.setGeometry(QtCore.QRect(630, 150, 211, 20))
        self.destinationPort.setObjectName("destinationPort")
        self.packetNumber = QtWidgets.QLineEdit(self.centralwidget)
        self.packetNumber.setGeometry(QtCore.QRect(630, 190, 211, 20))
        self.packetNumber.setObjectName("packetNumber")
        self.generateBtn = QtWidgets.QPushButton(self.centralwidget)
        self.generateBtn.setGeometry(QtCore.QRect(734, 230, 111, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.generateBtn.setFont(font)
        self.generateBtn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.generateBtn.setObjectName("generateBtn")
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(30, 271, 801, 251))
        self.listWidget.setObjectName("listWidget")
        self.clearBtn = QtWidgets.QPushButton(self.centralwidget)
        self.clearBtn.setGeometry(QtCore.QRect(770, 540, 75, 31))
        font = QtGui.QFont()
        font.setFamily("Segoe Print")
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.clearBtn.setFont(font)
        self.clearBtn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.clearBtn.setObjectName("clearBtn")
        UDPv4.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(UDPv4)
        self.statusbar.setObjectName("statusbar")
        UDPv4.setStatusBar(self.statusbar)

        self.retranslateUi(UDPv4)
        QtCore.QMetaObject.connectSlotsByName(UDPv4)

        # Function invoke
        self.generateBtn.clicked.connect(self.generate)
        self.clearBtn.clicked.connect(self.clearItems)
##################################################################
    def Packets(self, socket):
        print("abe")
        noTimeout = True
        while noTimeout:
            try:
                o = socket.recvfrom(1024)
                print(o)
                yield o
            except BaseException:
                return

    def is_valid_ipv4_address(self):
        try:
            socket.inet_pton(socket.AF_INET, self.address)
        except AttributeError:  # no inet_pton here, sorry
            try:
                socket.inet_aton(self.address)
            except socket.error:
                return False
            return self.address.count('.') == 3
        except socket.error:  # not a valid address
            return False
        return True
##################################################################
    def clearItems(self):
        self.listWidget.clear()
        self.listWidget.addItem("The source ip is: " + self.sourceIP.text())

    def generate(self):
        if not (self.sourcePort.text() == "" and self.sourceIP.text() == "" and
                self.destinationPort.text() == "" and self.destinationIP.text() == ""):
            self.listWidget.addItem("The source ip is: " + self.sourceIP.text())
            self.listWidget.addItem("The source port is: " + self.sourcePort.text())
            self.listWidget.addItem("The destination ip is: " + self.destinationIP.text())
            self.listWidget.addItem("The destination port is: " + self.destinationPort.text())
            self.listWidget.addItem("The number of packet is: " + self.packetNumber.text())
            self.listWidget.show()
            #self.sourceIP.setText("")
            # self.sourcePort.setText("")
            # self.destinationIP.setText("")
            # self.destinationPort.setText("")
            # self.packetNumber.setText("")
#############################################################3
            x = False
            self.address = self.destinationIP.text()
            self.PORTs = int(self.sourcePort.text())
            self.PORTr = int(self.destinationPort.text())
            self.pnum = int(self.packetNumber.text())
            print("3")
            if self.address.count('.') == 3:
                print("3")
                x = self.is_valid_ipv4_address()
                print('valid Ip, Packet is in sending ...')
            else:
                print("invalid ip address")
            if x == True:
                udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

                udp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 0)

                udp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

                udp_socket.settimeout(0.1)

                udp_socket.bind(('', self.PORTr))
                i = 0
                while udp_socket and i < self.pnum:
                    self.listWidget.addItem("sent packet to  " + self.address)
                    self.listWidget.show()
                    i = i + 1
                    message = " packet number ->%d Send to:" % i
                    print('sent packet %d' % i)
                    for data, address in self.Packets(udp_socket):
                        print(data, address)
                    udp_socket.sendto(message.encode('UTF'), (self.address, self.PORTs))
                    print("sent")
                    time.sleep(0.01)
                    #192.168.43.1
            else:
                print(" please Enter valid IP")


    def retranslateUi(self, UDPv4):
        _translate = QtCore.QCoreApplication.translate
        UDPv4.setWindowTitle(_translate("UDPv4", "UDPv4"))
        self.label.setText(_translate("UDPv4", "<html><head/><body><p><span style=\" "
                                               "font-size:16pt; font-weight:600;\">User "
                                               "Datagram Protocol "
                                               "Version-4</span></p></body></html>"))
        self.label_2.setText(_translate("UDPv4", "<html><head/><body><p><span style=\" "
                                                 "font-size:12pt; "
                                                 "font-weight:600;\">Source IP "
                                                 "Address</span></p></body></html>"))
        self.label_3.setText(_translate("UDPv4", "<html><head/><body><p><span style=\" "
                                                 "font-size:12pt; "
                                                 "font-weight:600;\">Source "
                                                 "Port</span></p></body></html>"))
        self.label_4.setText(_translate("UDPv4", "<html><head/><body><p><span style=\" "
                                                 "font-size:12pt; "
                                                 "font-weight:600;\">Destination IP "
                                                 "Address</span></p></body></html>"))
        self.label_5.setText(_translate("UDPv4", "<html><head/><body><p><span style=\" "
                                                 "font-size:12pt; "
                                                 "font-weight:600;\">Destination "
                                                 "Port</span></p></body></html>"))
        self.label_5.setText(_translate("UDPv4", "<html><head/><body><p><span style=\" "
                                                 "font-size:12pt; "
                                                 "font-weight:600;\">Destination "
                                                 "Port</span></p></body></html>"))
        self.label_6.setText(_translate("UDPv4", "<html><head/><body><p><span style=\" "
                                                 "font-size:12pt; "
                                                 "font-weight:600;\">Number of Packets"
                                                 "</span></p></body></html>"))
        self.generateBtn.setText(_translate("UDPv4", "Generate"))
        self.clearBtn.setText(_translate("UDPv4", "Clear"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    udp4 = QtWidgets.QMainWindow()
    ui = Ui_UDPv4()
    ui.setupUi(udp4)
    udp4.show()
    sys.exit(app.exec())