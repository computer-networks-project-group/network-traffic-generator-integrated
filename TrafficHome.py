# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'TrafficHome.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from FTP import Ui_FTP
from TCPv4 import Ui_TCPv4
# from TCPv6 import Ui_TCPv6
from HTTP import Ui_HTTP
from ICMPv4 import Ui_ICMPv4
# from ICMPv6 import Ui_ICMPv6
from UDPv4 import Ui_UDPv4
# from UDPv6 import Ui_UDPv6


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainvWindow")
        MainWindow.resize(800, 685)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tcpv4Radio = QtWidgets.QRadioButton(self.centralwidget)
        self.tcpv4Radio.setGeometry(QtCore.QRect(60, 170, 71, 17))
        font = QtGui.QFont()
        font.setFamily("Myanmar Text")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.tcpv4Radio.setFont(font)
        self.tcpv4Radio.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.tcpv4Radio.setObjectName("tcpv4Radio")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(50, 9, 641, 61))
        font = QtGui.QFont()
        font.setFamily("Sitka Text")
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        # self.tcpv6Radio = QtWidgets.QRadioButton(self.centralwidget)
        # self.tcpv6Radio.setGeometry(QtCore.QRect(190, 160, 71, 17))
        # font = QtGui.QFont()
        # font.setFamily("Myanmar Text")
        # font.setPointSize(12)
        # font.setBold(True)
        # font.setWeight(75)
        # self.tcpv6Radio.setFont(font)
        # self.tcpv6Radio.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        # self.tcpv6Radio.setObjectName("tcpv6Radio")
        self.udpv4Radio = QtWidgets.QRadioButton(self.centralwidget)
        self.udpv4Radio.setGeometry(QtCore.QRect(160, 170, 71, 17))
        font = QtGui.QFont()
        font.setFamily("Myanmar Text")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.udpv4Radio.setFont(font)
        self.udpv4Radio.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.udpv4Radio.setObjectName("udpv4Radio")
        # self.udpv6Radio = QtWidgets.QRadioButton(self.centralwidget)
        # self.udpv6Radio.setGeometry(QtCore.QRect(450, 160, 81, 17))
        # font = QtGui.QFont()
        # font.setFamily("Myanmar Text")
        # font.setPointSize(12)
        # font.setBold(True)
        # font.setWeight(75)
        # self.udpv6Radio.setFont(font)
        # self.udpv6Radio.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        # self.udpv6Radio.setObjectName("udpv6Radio")
        self.icmpv4Radio = QtWidgets.QRadioButton(self.centralwidget)
        self.icmpv4Radio.setGeometry(QtCore.QRect(260, 170, 81, 17))
        font = QtGui.QFont()
        font.setFamily("Myanmar Text")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.icmpv4Radio.setFont(font)
        self.icmpv4Radio.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.icmpv4Radio.setObjectName("icmpv4Radio")
        # self.icmpv6Radio = QtWidgets.QRadioButton(self.centralwidget)
        # self.icmpv6Radio.setGeometry(QtCore.QRect(190, 220, 81, 17))
        # font = QtGui.QFont()
        # font.setFamily("Myanmar Text")
        # font.setPointSize(12)
        # font.setBold(True)
        # font.setWeight(75)
        # self.icmpv6Radio.setFont(font)
        # self.icmpv6Radio.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        # self.icmpv6Radio.setObjectName("icmpv6Radio")
        self.httpRadio = QtWidgets.QRadioButton(self.centralwidget)
        self.httpRadio.setGeometry(QtCore.QRect(360, 170, 71, 17))
        font = QtGui.QFont()
        font.setFamily("Myanmar Text")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.httpRadio.setFont(font)
        self.httpRadio.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.httpRadio.setObjectName("httpRadio")
        self.ftpRadio = QtWidgets.QRadioButton(self.centralwidget)
        self.ftpRadio.setGeometry(QtCore.QRect(460, 170, 71, 17))
        font = QtGui.QFont()
        font.setFamily("Myanmar Text")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.ftpRadio.setFont(font)
        self.ftpRadio.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.ftpRadio.setObjectName("ftpRadio")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(40, 60, 441, 81))
        font = QtGui.QFont()
        font.setFamily("Sitka Text")
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignTop)
        self.label_2.setObjectName("label_2")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        # Function calls
        self.tcpv4Radio.clicked.connect(self.tcp4)
        # self.tcpv6Radio.clicked.connect(self.tcp6)
        self.udpv4Radio.clicked.connect(self.udp4)
        # self.udpv6Radio.clicked.connect(self.udp6)
        self.icmpv4Radio.clicked.connect(self.icmp4)
        # self.icmpv6Radio.clicked.connect(self.icmp6)
        self.httpRadio.clicked.connect(self.http)
        self.ftpRadio.clicked.connect(self.ftp)

    def tcp4(self):
        # TCPv4 ui
        self.tcp_4 = QtWidgets.QMainWindow()
        self.ui = Ui_TCPv4()
        self.ui.setupUi(self.tcp_4)
        self.tcp_4.show()

    # def tcp6(self):
    #     # TCPv6 ui
    #     self.tcp_6 = QtWidgets.QMainWindow()
    #     self.ui = Ui_TCPv6()
    #     self.ui.setupUi(self.tcp_6)
    #     self.tcp_6.show()

    def icmp4(self):
        # ICMPv4 ui
        self.icmp_4 = QtWidgets.QMainWindow()
        self.ui = Ui_ICMPv4()
        self.ui.setupUi(self.icmp_4)
        self.icmp_4.show()

    # def icmp6(self):
    #     # ICMPv6 ui
    #     self.icmp_6 = QtWidgets.QMainWindow()
    #     self.ui = Ui_ICMPv6()
    #     self.ui.setupUi(self.icmp_6)
    #     self.icmp_6.show()

    def udp4(self):
        # UDPv4 ui
        self.udp_4 = QtWidgets.QMainWindow()
        self.ui = Ui_UDPv4()
        self.ui.setupUi(self.udp_4)
        self.udp_4.show()

    # def udp6(self):
    #     # UDPv6 ui
    #     self.udp_6 = QtWidgets.QMainWindow()
    #     self.ui = Ui_UDPv6()
    #     self.ui.setupUi(self.udp_6)
    #     self.udp_6.show()

    def http(self):
        # HTTP ui
        self.https = QtWidgets.QMainWindow()
        self.ui = Ui_HTTP()
        self.ui.setupUi(self.https)
        self.https.show()

    def ftp(self):
        # FTP ui
        self.ftps = QtWidgets.QMainWindow()
        self.ui = Ui_FTP()
        self.ui.setupUi(self.ftps)
        self.ftps.show()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Network  Traffic Generator"))
        self.tcpv4Radio.setText(_translate("MainWindow", "TCPv4"))
        self.label.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" "
                                                    "font-size:26pt; "
                                                    "color:#ff0000;\">Python Network "
                                                    "Traffic "
                                                    "Generator</span></p></body></html>"))
        # self.tcpv6Radio.setText(_translate("MainWindow", "TCPv6"))
        self.udpv4Radio.setText(_translate("MainWindow", "UDPv4"))
        # self.udpv6Radio.setText(_translate("MainWindow", "UDPv6"))
        self.icmpv4Radio.setText(_translate("MainWindow", "ICMPv4"))
        # self.icmpv6Radio.setText(_translate("MainWindow", "ICMPv6"))
        self.httpRadio.setText(_translate("MainWindow", "HTTP"))
        self.ftpRadio.setText(_translate("MainWindow", "FTP"))
        self.label_2.setText(_translate("MainWindow", "<html><head/><body><p><span "
                                                      "style=\" font-size:48pt; "
                                                      "text-decoration: underline; "
                                                      "color:#aa00ff;\">Protocols</span"
                                                      "></p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    home = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(home)
    home.show()
    sys.exit(app.exec())
