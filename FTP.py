# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'HTTP.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from ftplib import FTP
import os

class Ui_HTTP(object):
    #declare global variables  
    srcip=""
    dstip=""
    srcPort=""
    dstPort=""
    numP=""
    ip=""
    get=""
    def setupUi(self, HTTP):
        HTTP.setObjectName("HTTP")
        HTTP.resize(858, 600)
        self.centralwidget = QtWidgets.QWidget(HTTP)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(120, 20, 321, 31))
        font = QtGui.QFont()
        font.setFamily("Ebrima")
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(20, 95, 151, 21))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(70, 140, 101, 21))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(476, 98, 151, 21))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(526, 138, 101, 21))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(470, 170, 151, 21))
        font = QtGui.QFont()
        font.setFamily("MS Sans Serif")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.sourceIP = QtWidgets.QLineEdit(self.centralwidget)
        self.sourceIP.setGeometry(QtCore.QRect(180, 100, 191, 20))
        self.sourceIP.setObjectName("sourceIP")
        self.sourcePort = QtWidgets.QLineEdit(self.centralwidget)
        self.sourcePort.setGeometry(QtCore.QRect(180, 140, 191, 20))
        self.sourcePort.setObjectName("sourcePort")
        self.destinationIP = QtWidgets.QLineEdit(self.centralwidget)
        self.destinationIP.setGeometry(QtCore.QRect(636, 98, 191, 20))
        self.destinationIP.setObjectName("destinationIP")
        self.destinationPort = QtWidgets.QLineEdit(self.centralwidget)
        self.destinationPort.setGeometry(QtCore.QRect(636, 138, 191, 20))
        self.destinationPort.setObjectName("destinationPort")
        self.packetNumber = QtWidgets.QLineEdit(self.centralwidget)
        self.packetNumber.setGeometry(QtCore.QRect(636, 170, 191, 20))
        self.packetNumber.setObjectName("destinationPort")
        self.generateBtn = QtWidgets.QPushButton(self.centralwidget)
        self.generateBtn.setGeometry(QtCore.QRect(720, 200, 101, 31))
        font = QtGui.QFont()
        font.setFamily("Myanmar Text")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.generateBtn.setFont(font)
        self.generateBtn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.generateBtn.setObjectName("generateBtn")
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(30, 270, 801, 241))
        self.listWidget.setObjectName("listWidget")
        self.clearBtn = QtWidgets.QPushButton(self.centralwidget)
        self.clearBtn.setGeometry(QtCore.QRect(734, 532, 81, 31))
        font = QtGui.QFont()
        font.setFamily("Sitka")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.clearBtn.setFont(font)
        self.clearBtn.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.clearBtn.setObjectName("clearBtn")
        HTTP.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(HTTP)
        self.statusbar.setObjectName("statusbar")
        HTTP.setStatusBar(self.statusbar)

        self.retranslateUi(HTTP)
        QtCore.QMetaObject.connectSlotsByName(HTTP)

        self.generateBtn.clicked.connect(self.generate)
        self.clearBtn.clicked.connect(self.clearItems)

    def clearItems(self):
        self.listWidget.clear()

    def generate(self):
        if not (self.sourcePort.text() == "" and self.sourceIP.text() == "" and
                self.destinationPort.text() == "" and self.destinationIP.text() == "" and
                self.packetNumber.text() == ""):
            self.listWidget.addItem("The source ip is: " + self.sourceIP.text())
            self.listWidget.addItem("The source port is: " + self.sourcePort.text())
            self.listWidget.addItem("The destination ip is: " + self.destinationIP.text())
            self.listWidget.addItem("The destination port is: " + self.destinationPort.text())
            self.listWidget.addItem("The number of packets is: " +
                                    self.packetNumber.text())
            # Prepare GET statement header
            self.get='GET / HTTP/1.1\r\nHost: www.google.com\r\nUser-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:10.0) Gecko/20100101 Firefox/10.0\r\nConnection: keep-alive,\r\nAccept-Language: en\r\n\r\n'

            #target IP(host) or accept from input field
            self.srcip=self.sourceIP.text()
            self.dstip=self.destinationIP.text()
            self.srcPort=self.sourcePort.text()
            self.dstPort=self.destinationPort.text()
            self.numP=self.packetNumber.text()
            self.ip=self.IP(src=srcip, dst=dstip)
            
            
            self.x = False
            if self.dstip.count('.') == 3:    
                self.x = self.is_valid_ipv4_address(dstip)
            else:
                self.listWidget.addItem("invalid ip address "+ self.destinationIP.text() +". please insert valid ip address?")
            if self.x == True:
                self.ports = self.IsPortOn(dstip,80)
                if self.ports == True:
                    for self.i in range(self.numP):
                        self.i=self.i+1
                        self.httpRequest()
                else: 
                    self.listWidget.addItem("Port "+ self.destinationPort.text() +"is closed . please try again?")
            self.sourceIP.setText("")
            self.sourcePort.setText("")
            self.destinationIP.setText("")
            self.destinationPort.setText("")
            self.packetNumber.setText("")
        else:
            self.listWidget.addItem("sorry,you have to fill the fields.")
            
    def httpRequest(self):
        self.port=self.srcPort

        SYN=self.ip/self.TCP(sport=self.port, dport=self.dstPort, flags="S", seq=42)
        
        SYNACK=self.sr1(SYN)
        ACK=self.ip/self.TCP(sport=SYNACK.dport, dport=80, flags="A", seq=SYNACK.ack, ack=SYNACK.seq + 1) / self.get
        
        self.listWidget.addItem("Sending ACK-GET packet")
        self.reply,self.error=sr(ACK)
        
        self.listWidget.addItem(" Reply from server:")
        self.listWidget.addItem(reply.show())

        self.listWidget.addItem('Done!')
    
    def is_valid_ipv4_address(self,dstip):
        try:
            socket.inet_pton(socket.AF_INET, dstip)
        except AttributeError:  # no inet_pton here, sorry
            try:
                socket.inet_aton(dstip)
            except socket.error:
                return False
            return dstip.count('.') == 3 
        except socket.error:  # not a valid address
            return False
        return True

    def IsPortOn(self, ip, port):
        try:
            s.connect((ip, port))
            return True
        except:
            return None
            
    def retranslateUi(self, HTTP):
        _translate = QtCore.QCoreApplication.translate
        HTTP.setWindowTitle(_translate("HTTP", "HyperTExt Transfer Protocol"))
        self.label.setText(_translate("HTTP", "<html><head/><body><p><span style=\" "
                                            "font-style:italic;\">HyperText Transfer "
                                            "Protocol</span></p></body></html>"))
        self.label_2.setText(_translate("HTTP", "Source Ip Address"))
        self.label_3.setText(_translate("HTTP", "Source Port"))
        self.label_4.setText(_translate("HTTP", "Source Ip Address"))
        self.label_5.setText(_translate("HTTP", "Source Port"))
        self.label_6.setText(_translate("HTTP", "Number of Packets"))
        self.generateBtn.setText(_translate("HTTP", "Generate"))
        self.clearBtn.setText(_translate("HTTP", "Clear"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    http = QtWidgets.QMainWindow()
    ui = Ui_HTTP()
    ui.setupUi(http)
    http.show()
    sys.exit(app.exec())
